.. cryobf documentation master file, created by
   sphinx-quickstart on Tue Jul 19 19:22:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cryobf's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/api
   usage/tutorial


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
